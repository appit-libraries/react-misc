import classnames from 'classnames';
import React, {Ref} from 'react';
import {connectField, HTMLFieldProps} from 'uniforms';
import omit from 'lodash/omit';

import wrapField from './wrapField2';

export type BoolFieldProps = HTMLFieldProps<boolean,
	HTMLDivElement,
	{
		inline?: boolean;
		inputClassName?: string;
		inputRef?: Ref<HTMLInputElement>;
		labelBefore?: string;
	}>;

function Bool({onChange, ...props}: BoolFieldProps){
	const {
		disabled,
		error,
		inline,
		inputClassName,
		inputRef,
		label,
		labelBefore,
		name,
		value,
		is_switch
	} = props;
	return wrapField(
		omit({...props, label: labelBefore, value: props.value},['is_switch']),
		<div
			className={classnames(inputClassName,"custom-control", `custom-${is_switch ? 'switch' : 'checkbox'}`, {
				'text-danger': error,
				'custom-control-inline': inline,
			})}
		>
			<input
				checked={value || false}
				className="custom-control-input"
				disabled={disabled}
				id={props.id}
				name={name}
				onChange={() => onChange(!value)}
				ref={inputRef}
				type="checkbox"
			/>
			<label htmlFor={props.id} className="custom-control-label">
				{label}
			</label>
		</div>,
	);
}

export default connectField(Bool, {kind: 'leaf'});
