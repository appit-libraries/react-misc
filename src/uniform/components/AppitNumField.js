
import wrapField2 from './wrapField2';
import classnames from 'classnames';
import omit from 'lodash/omit';
import React from 'react';
import {connectField} from 'uniforms';
import {NumFieldProps} from 'uniforms-bootstrap4';

export function NumInput(props){
	return (
		<input
			className={classnames(props.inputClassName, 'form-control', {
				'is-invalid': props.error,
			})}
			disabled={typeof props.disabled === 'function' ? props.disabled(props) : props.disabled}
			onWheel={e => {e.target.blur();}}
			id={props.id}
			max={props.max}
			min={props.min}
			name={props.name}
			onChange={event => {
				const parse = props.decimal ? parseFloat : parseInt;
				const value = parse(event.target.value);
				props.onChange(isNaN(value) ? undefined : value);
			}}
			placeholder={props.placeholder}
			ref={props.inputRef}
			step={props.step || (props.decimal ? 0.01 : 1)}
			type="number"
			value={props.value ?? ''}
		/>
	);
}

function Num(props: NumFieldProps){
	let label = typeof props.field.label === 'function' ? props.field.label(props) : props.field.label;

	return wrapField2(
		omit({...props, label}, ['value', 'onChange', 'decimal']),
		NumInput(props),
	);
}

export default connectField(Num, {kind: 'leaf'});