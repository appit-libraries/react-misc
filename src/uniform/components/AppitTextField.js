import classnames from 'classnames';
import omit from 'lodash/omit';
import React, {Ref} from 'react';
import wrapField2 from './wrapField2';
import {connectField, HTMLFieldProps} from 'uniforms';
import InputMask from 'react-input-mask';

export type TextFieldProps = HTMLFieldProps<
	string,
	HTMLInputElement,
	{ inputClassName?: string; inputRef?: Ref<HTMLInputElement>; mask?:boolean }
	>;

function Text(props: TextFieldProps) {
	const Tag = props.mask ? InputMask : 'input';


	return wrapField2(
		omit(props, ['value', 'onChange','inputProps','mask']),
		<Tag
			{...(props.inputProps || {})}
			className={classnames(props.inputClassName, 'form-control', {
				'is-invalid': props.error,
			})}
			disabled={props.disabled}
			id={props.id}
			name={props.name}
			onChange={event => props.onChange(event.target.value)}
			placeholder={props.placeholder}
			ref={props.inputRef}
			type={props.type ?? 'text'}
			value={props.value ?? ''}
		/>,
	);
}

export default connectField(Text, { kind: 'leaf' });
