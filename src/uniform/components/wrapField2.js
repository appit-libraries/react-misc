import classnames from 'classnames';
import omit from 'lodash/omit';
import React, {HTMLProps, ReactNode} from 'react';
import {filterDOMProps, Override} from 'uniforms';
import {gridClassName} from 'uniforms-bootstrap4';

type WrapperProps = Override<
	Omit<HTMLProps<HTMLDivElement>, 'onChange'>,
	{
		error?: unknown;
		errorMessage?: string;
		grid?: number | string | Record<string, number>;
		help?: string;
		helpClassName?: string;
		label?: ReactNode;
		labelClassName?: string | string[];
		showInlineError?: boolean;
		value?: boolean | string | number | string[] | undefined;
		wrapClassName?: string;
	}
	>;

export default function wrapField2(
	{
		className,
		disabled,
		error,
		errorMessage,
		name,
		grid, // Grid is either an number between 1 and 11 or an object with keys like xs and md.
		help, // Help text.
		helpClassName, // Help text class name.
		id,
		label,
		labelClassName, // Label class name (String|Array[String]).
		required,
		showInlineError, // Show inline error message?
		wrapClassName, // Input wrapper class name.
		forceNoGrid,
		helpClassless,
		...props
	}: WrapperProps,
	children: ReactNode,
) {
	const hasWrap = !!(grid || wrapClassName) && !forceNoGrid;
	const blockError = !!(error && showInlineError) && (
		<span className="form-text text-danger">{errorMessage}</span>
	);
	const blockHelp = !!help && (
		<span className={classnames({'form-text':!helpClassless}, helpClassName || 'text-muted')}>
			{typeof help === 'function' ? help() : help}
		</span>
	);

	return (
		<div
			className={classnames(className, 'form-group', {
				'is-invalid': error,
				disabled,
				required,
				row: grid,
			})}
			{...filterDOMProps(
				omit(props, [
					'checkboxes',
					'inline',
					'inputClassName',
					'labelClassName',
					'inputRef',
					'rows',
					'transform',
					'forceNoGrid',
					'helpClassless',
				]),
			)}
		>
			{label && (
				<label
					htmlFor={id}
					className={classnames(
						{
							'col-form-label': grid,
							'text-danger': error,
						},
						gridClassName(grid, 'label'),
						labelClassName,
					)}
				>
					{label}
				</label>
			)}

			{hasWrap && (
				<div
					className={classnames(
						wrapClassName,
						gridClassName(grid, 'input')
					)}
				>
					{children}
					{blockHelp}
					{blockError}
				</div>
			)}

			{!hasWrap && children}
			{!hasWrap && blockHelp}
			{!hasWrap && blockError}
		</div>
	);
}
