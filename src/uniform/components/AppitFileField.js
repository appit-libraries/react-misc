import wrapField2 from './wrapField2';
import classnames from 'classnames';
import {omit} from 'lodash';
import React, {useState} from 'react';
import {connectField} from 'uniforms';

AppitFile.propTypes = {};

function AppitFile({id, inputClassName, className, error, label, placeholder, name, contentMediaType, onChange, onlyBase64,default_src, ...props}){
	const [currentLabel, setCurrentLabel] = useState(placeholder || label);
	const onImageChange = ({target: {files}}) => {
		if(files && files[0]){
			setCurrentLabel(files[0].name);
			var reader = new FileReader();
			reader.onloadend = function(){
				let result = reader.result;
				// console.log(result)
				if(onlyBase64){
					result = result.split('base64,')[1];
				}
				onChange(result);
			};
			reader.readAsDataURL(files[0]);
		}
	};

	const isImage = contentMediaType?.match('image');

	return wrapField2(
		omit({...props, className, id, label}, [
			'options',
			'transformLabel',
			'contentEncoding',
			'onlyBase64',
			'isImage',
		]), (<div className="d-flex flex-row">
			{isImage && <img
				className="form-control-file-preview mr-1"
				src={props.value ? `data:${contentMediaType};base64,${props.value}` : default_src}
				alt=""
			/>}
			<div
				className={classnames(inputClassName, 'form-control form-control-file flex-grow-1', {
					'is-invalid': error,
				})}
			>
				<div className="custom-file">
					<input
						onChange={onImageChange}
						type="file"
						name={name}
						className="custom-file-input"
						accept={contentMediaType}
						id="file"
					/>
					<label className="custom-file-label" htmlFor="file">
						{currentLabel}
					</label>
				</div>
			</div>
		</div>),
	);
}

export default connectField(AppitFile, {kind: 'leaf'});