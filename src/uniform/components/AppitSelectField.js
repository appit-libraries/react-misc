import classnames from 'classnames';
import {omit} from 'lodash';
import React, {useMemo, useState} from 'react';
import {createFilter} from 'react-select';
import {connectField} from 'uniforms';
import ReactSelect from './ReactSelect';
import wrapField2 from './wrapField2';

const base64 =
	typeof btoa !== 'undefined'
		? btoa
		: (x: string) => Buffer.from(x).toString('base64');
const escape = (x: string) => base64(encodeURIComponent(x)).replace(/=+$/, '');

const xor = (item, array) => {
	const index = array.indexOf(item);
	if(index === -1){
		return array.concat([item]);
	}

	return array.slice(0, index).concat(array.slice(index + 1));
};

function Select({
					allowedValues,
					checkboxes,
					className,
					disabled,
					error,
					containerClassName,
					errorMessage,
					fieldType,
					id,
					inline,
					inputClassName,
					inputRef,
					label,
					name,
					onChange,
					placeholder,
					multiselect,
					required,
					showInlineError,
					transform,
					value,
					tokens,
					search,
					searchNotFoundMessage = 'Nada encontrado',
					add,
					inputSearchClassName = '',
					searchClassName = '',
					addClassName = '',
					transformLabel = (v) => v,
					...props
				}: SelectFieldProps){

	const [newValues, setNewValues] = useState([]);
	const values = useMemo(() => {
		return [...allowedValues, ...newValues];
	}, [allowedValues, newValues]);
	if(!tokens){
		tokens = (a) => {
			const option = props.options.find(({value}) => value === a);

			if(!!option){
				return option.tokens || [];
			}

			return [];
		};
	}
	const options = (allowedValues || []).map(a => {
		const label = transformLabel(transform ? transform(a) : a);
		return {
			value: a,
			label: typeof label === 'function' ? label() : label,
			tokens: tokens ? tokens(a) : null,
		};
	});
	let defaultValue;

	if(fieldType === Array){
		defaultValue = (value || []).map(value => options.find(o => o.value === value));
	}
	else{
		defaultValue = options.find(o => o.value === value);
	}

	const [query, setQuery] = useState('');
	const findOption = value => {
		const option = options.find(o => o.value === value);
		if(!option){
			return {
				value,
				label: value,
			};
		}

		return option;
	};
	const filter = createFilter({
		stringify: option => {
			if(!option.data.tokens){
				return `${option.label} ${option.value}`;
			}

			return option.data.tokens.join(' ');
		},
	});
	const findedValues = search ? values.filter(item => {
		const option = findOption(item);

		return filter({...option, data: option}, query);
	}) : values;
	const addValue = () => {
		onChange(fieldType === Array ? xor(query, value) : query);
		setNewValues([...newValues, query]);
		setQuery('');
	};

	return wrapField2(
		omit({...props, className, id, label}, [
			'options',
			'transformLabel',
			'tokens',
			'items',
			'search',
			'searchClassName',
			'add',
			'decimal',
			'addClassName',
			'inputSearchClassName',
			'containerClassName',
			'multiselect',
			'minCount',
			'maxCount'
		]),
		checkboxes || (fieldType === Array && !multiselect) ? (
			<>
				{search && (
					<div className={`form-group ${searchClassName}`}>
						<input
							value={query}
							type="text"
							placeholder="Pesquise..."
							className={`form-control ${inputSearchClassName}`}
							onChange={e => setQuery(e.target.value)}
						/>
					</div>
				)}
				<div className={containerClassName}>
					{values?.map(item => {
						const label = newValues.includes(item) ? item : (transform ? transform(item) : item);
						const option = findOption(item);
						const show = filter({...option, data: option}, query);
						// console.log(item,value?.includes(item),value)
						return (
							<div
								key={item}
								className={classnames(
									inputClassName,
									{'d-none': !show},
									{'d-inline': show && inline}
								)}
							>
								<div
									className={classnames(
										'custom-control custom-checkbox',
										{'custom-control-inline': inline},
									)}
								>
									<input
										checked={
											fieldType === Array ? value?.includes(item) : value === item
										}
										className="custom-control-input"
										disabled={disabled}
										id={`${id}-${escape(item)}`}
										name={name}
										onChange={() => {
											onChange(fieldType === Array ? xor(item, value) : item);
										}}
										type="checkbox"
									/>
									<label htmlFor={`${id}-${escape(item)}`} className="custom-control-label">
										{typeof label === 'function' ? label(props) : label}
									</label>
								</div>
							</div>
						);
					})}
				</div>
				{findedValues.length === 0 && (<div className={addClassName}>
					<div className="text-muted h5 text-uppercase">
						{!query
							? searchNotFoundMessage
							: (<>
								{searchNotFoundMessage} com "<i>{query}</i>"
							</>)
						}
					</div>
					{add && (
						<button className="btn btn-info" onClick={addValue} disabled={!query}>
							Adicionar
						</button>
					)}
				</div>)}
			</>
		) : (
			<ReactSelect
				className={classnames(inputClassName, 'form-control', {
					'is-invalid': error,
				})}
				onChange={option => {
					const toValue = (option) => option?.value;

					onChange(fieldType === Array ? option ? option.map(toValue) : [] : toValue(option));
				}}
				ref={inputRef}
				isLoading={!disabled && options.length === 0}
				isDisabled={disabled}
				value={defaultValue}
				isClearable={!required}
				isMulti={!!multiselect}
				// defaultInputValue={value}
				defaultValue={defaultValue}
				placeholder={placeholder || label}
				options={options}
			/>
		),
	);
}

export default connectField(Select, {kind: 'leaf'});
