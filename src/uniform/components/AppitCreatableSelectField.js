import ReactCreatableSelect from './ReactCreatableSelect';
import wrapField2 from './wrapField2';
import classnames from 'classnames';
import omit from 'lodash/omit';
import {connectField} from 'uniforms';1
import React from 'react';

function AppitCreatableSelect({
				 allowedValues,
				 checkboxes,
				 className,
				 disabled,
				 error,
				 errorMessage,
				 fieldType,
				 id,
				 inline,
				 inputClassName,
				 inputRef,
				 label,
				 name,
				 onChange,
				 placeholder,
				 required,
				 showInlineError,
				 transform,
				 value,
				 transformLabel=(v) => v,
				 ...props
			 }: SelectFieldProps){

	const options = (allowedValues || []).map(a => {
		const label = transformLabel(transform ? transform(a) : a);
		return {
			value: a,
			label: typeof label === 'function' ? label() : label,
		}
	});
	const defaultValue = options.find(o => o.value === value);

	return wrapField2(
		omit({...props, className, id, label},[
			"options",
			"transformLabel",
		]),
		<ReactCreatableSelect
			className={classnames(inputClassName, 'form-control', {
				'is-invalid': error,
			})}
			onChange={({value}) => onChange(value)}
			ref={inputRef}
			isLoading={options.length === 0}
			isDisabled={disabled}
			value={defaultValue}
			// defaultInputValue={value}
			defaultValue={defaultValue}
			// onCreateOption={value => console.log(allowedValues)}
			placeholder={placeholder || label}
			options={options}
		/>,
	);
}

export default connectField(AppitCreatableSelect, {kind: 'leaf'});