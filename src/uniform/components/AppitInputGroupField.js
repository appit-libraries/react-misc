import {omit} from 'lodash';
// import React from 'react';
import {connectField} from 'uniforms';
import {NumFieldProps} from 'uniforms-bootstrap4';
import {NumInput} from './AppitNumField';
import wrapFieldInInputGroup from './wrapFieldInInputGroup2';

function Num(props: NumFieldProps){
	let label = typeof props.field.label === 'function' ? props.field.label(props) : props.field.label;
	let prepend = typeof props.field.prepend === 'function' ? props.field.prepend(props) : props.field.prepend;
	let append = typeof props.field.append === 'function' ? props.field.append(props) : props.field.append;
	if(!append){
		append = props.label;
	}

	return wrapFieldInInputGroup(
		omit({...props, append, prepend, label}, ['value', 'onChange', 'decimal']),
		NumInput(props),
	);
}

export default connectField(Num, {kind: 'leaf'});