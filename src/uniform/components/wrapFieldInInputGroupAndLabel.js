import classnames from 'classnames';
import {omit} from 'lodash';
import React, {HTMLProps, ReactNode} from 'react';
import {InputGroup} from 'react-bootstrap';
import {filterDOMProps, Override} from 'uniforms';

type WrapperProps = Override<Omit<HTMLProps<HTMLDivElement>, 'onChange'>,
	{
		error?: unknown;
		errorMessage?: string;
		grid?: number | string | Record<string, number>;
		help?: string;
		helpClassName?: string;
		label?: ReactNode;
		labelClassName?: string | string[];
		showInlineError?: boolean;
		value?: boolean | string | number | string[] | undefined;
		wrapClassName?: string;
		color?: string;
		groupsClassName?: string;
		group?: string | (props: WrapperProps) => any;
		prepend?: boolean | (props: WrapperProps) => any;
	}>;

export default function wrapFieldInInputGroupAndLabel(
	{
		className,
		disabled,
		error,
		errorMessage,
		grid, // Grid is either an number between 1 and 11 or an object with keys like xs and md.
		help, // Help text.
		helpClassName, // Help text class name.
		id,
		label,
		labelClassName, // Label class name (String|Array[String]).
		required,
		showInlineError, // Show inline error message?
		wrapClassName, // Input wrapper class name.
		color,
		group,
		prepend,
		groupsClassName,
		...props
	}: WrapperProps,
	children: ReactNode,
){
	// const hasWrap = !!(grid || wrapClassName);
	const blockError = !!(error && showInlineError) && (
		<span className="form-text text-danger">{errorMessage}</span>
	);
	help = typeof help === 'function' ? help() : help;
	const blockHelp = !!help && (
		<span className={classnames('form-text', helpClassName || 'text-muted')}>
			{help}
		</span>
	);

	const Group = prepend ? InputGroup.Prepend : InputGroup.Append;

	return (
		<div
			className={classnames(className, 'form-group', {
				'is-invalid': error,
				disabled,
				required,
				row: grid,
			})}
			{...filterDOMProps(
				omit(props, [
					'checkboxes',
					'inline',
					'inputClassName',
					'groupsClassName',
					'inputRef',
					'rows',
					'transform',
					'color',
					'group',
					'prepend',
				]),
			)}
		>
			{label && (
				<label
					htmlFor={id}
					className={classnames(
						{
							'col-form-label': grid,
							'text-danger': error,
						},
						// gridClassName(grid, 'label'),
						labelClassName,
					)}
				>
					{label}
				</label>
			)}
			<InputGroup>
				{!prepend && children}
				{group !== undefined && (
					<Group>
						<InputGroup.Text
							as="label" htmlFor={id}
							className={classnames(
								{
									'col-form-label': grid,
									'text-danger': error,
								},
								groupsClassName,
								typeof color === 'function' ? color(props) : color,
							)}
						>
							{typeof group === 'function' ? group(props) : group}
						</InputGroup.Text>
					</Group>
				)}
				{prepend && children}
			</InputGroup>
			{blockHelp}
			{blockError}
		</div>
	);
}