import classnames from 'classnames';
import React from 'react';
import LoginError from '../LoginError';
import {filterDOMProps, useForm} from 'uniforms';
import {gridClassName} from 'uniforms-bootstrap4';

AppitSubmitField.defaultProps = {
	submittingText: <i className="fa fa-spin fa-spinner"/>
}

export function AppitSubmitField({
								className,
								disabled,
								inputClassName,
								inputRef,
								name,
								children,
								wrapClassName,
								submittingText,
	grid,
	inputProps={},
								...props
							}: SubmitFieldProps){
	const {error, state, submitting, ...extra} = useForm();
	const hasWrap = !!((grid === false ? false : state.grid) || wrapClassName);

	const blockInput = (
		<button
			{...inputProps}
			className={classnames('btn btn-primary font-weight-bold px-9 py-4', inputClassName)}
			disabled={disabled === undefined ? !!((error && !(error instanceof LoginError)) || state.disabled) : disabled}
			ref={inputRef}
			type="submit"
		>
			<span>
				{children}
			</span>
			{submitting && submittingText}
		</button>
	);

	return (
		<div
			className={classnames(className, {
				'is-invalid': error,
				row: grid === false ? false : state.grid,
			})}
			{...filterDOMProps(props)}
		>
			{hasWrap && (
				<label
					className={classnames(
						'col-form-label',
						gridClassName(state.grid, 'label'),
					)}
				>
					&nbsp;
				</label>
			)}
			{hasWrap && (
				<div
					className={classnames(
						wrapClassName,
						gridClassName(state.grid, 'input'),
					)}
				>
					{blockInput}
				</div>
			)}
			{!hasWrap && blockInput}
		</div>
	);
}