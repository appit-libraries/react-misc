import React, {Ref} from 'react';
import classnames from 'classnames';
import {connectField, HTMLFieldProps} from 'uniforms';
import wrapField from './wrapField2';

export type BoolFieldProps = HTMLFieldProps<boolean,
	HTMLDivElement,
	{
		inline?: boolean;
		inputClassName?: string;
		inputRef?: Ref<HTMLInputElement>;
		labelBefore?: string;
	}>;

function Bool({onChange, ...props}: BoolFieldProps){
	const {
		disabled,
		error,
		inline,
		inputClassName,
		inputRef,
		labelBefore,
		name,
		value,
	} = props;
	let label = typeof props.field.label === 'function' ? props.field.label(props) : props.field.label;
	return wrapField(
		{...props, label: labelBefore, value: props.value},
		<div
			className={classnames(inputClassName, 'custom-control', 'custom-switch', {
				'text-danger': error,
				'custom-control-inline': inline,
			})}
		>
			<input
				checked={value || false}
				className="custom-control-input"
				disabled={disabled}
				id={props.id}
				name={name}
				onChange={() => onChange(!value)}
				ref={inputRef}
				type="checkbox"
			/>
			<label htmlFor={props.id} className="custom-control-label">
				{label}
			</label>
		</div>,
	);
}

export default connectField(Bool, {kind: 'leaf'});
