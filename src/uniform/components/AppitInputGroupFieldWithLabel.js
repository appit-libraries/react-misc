import {NumInput} from './AppitNumField';
import wrapFieldInInputGroupAndLabel from './wrapFieldInInputGroupAndLabel';
import omit from 'lodash/omit';
// import React from 'react';
import {connectField} from 'uniforms';
import {NumFieldProps} from 'uniforms-bootstrap4';


function Num(props: NumFieldProps) {
	let label = typeof props.field.label === 'function' ? props.field.label(props) : props.field.label;

	return wrapFieldInInputGroupAndLabel(
		omit({...props,label}, ['value', 'onChange','decimal','color']),
		NumInput(props),
	);
}

export default connectField(Num, { kind: 'leaf' });