import Ajv from 'ajv';
import * as immutable from 'object-path-immutable';
import PropTypes from 'prop-types';
import React from 'react';
import {AutoForm} from 'uniforms-bootstrap4';
import {JSONSchemaBridge} from 'uniforms-bridge-json-schema';
import {AppitAlertBasic} from '../alert/AppitAlert';

class AppitUniform extends React.Component {
	static propTypes = {
		ended: PropTypes.bool.isRequired,
		started: PropTypes.bool.isRequired,
		error: PropTypes.any,
		send: PropTypes.func.isRequired,
		after: PropTypes.func,
	};
	static defaultProps = {};
	getInitialState(){
		return {ack: false}
	}

	state = this.getInitialState();

	shouldAlert(){
		return true;
	}

	getLoader(){
		return 'Loading...';
	}

	getSchema(){
		return {};
	}

	getAlertCallback(){
		return AppitAlertBasic;
	}

	alert(title, message){
		const alert = this.getAlertCallback();

		alert(title, message, () => this.setState({ack: true}));
	}

	alertOnSuccess(title='Operação concluida',message='Dados enviados com sucesso!'){
		this.alert(title, message);
	}

	errorsToMessage(error){
		return 'Dados não foram enviados!';
	}

	alertOnFailure(message="Operação Falhou"){
		this.alert(message, this.errorsToMessage(this.props.error));
	}

	componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void{
		if(this.shouldAlert()){
			if(this.props.started && this.props.ended && !prevProps.ended){
				if(this.props.error){
					this.alertOnFailure();
				}
				else{
					this.alertOnSuccess();
				}
			}
			if(this.state.ack && !prevState.ack && !this.props.error){
				if(typeof this.props.after === 'function'){
					this.props.after();
				}
			}
		}
	}

	handleData(data){
		return data;
	}

	onSubmit(data){
		this.props.send(this.handleData(data));
		if(this.shouldAlert()){
			this.setState({ack: false});
		}
	}

	form(bridge, onSubmit){
		return <AutoForm
			placeholder={true}
			schema={bridge}
			onSubmit={onSubmit}
		/>;
	}


	render(){
		const {started, ended} = this.props;
		const {ack} = this.state.ack;
		if(started && !ended && !ack){
			return this.getLoader();
		}

		const bridge = createBridge(this.getSchema());

		return this.form(bridge, (data) => this.onSubmit(data));
	}
}

export const createBridge = (schema) => {
	const ajv = new Ajv({allErrors: true, useDefaults: true});
	const validator = ajv.compile(schema);

	const validadorCallback = (model,...props) => {
		// console.log(props)
		validator(model);

		if(validator.errors && validator.errors.length){
			return {details: validator.errors};
		}
	};
	const merge = (properties) => {
		for(const property in properties){
			const schemaProperty = properties[property];
			if(!(schemaProperty.type in ['object', 'array'])){
				properties[property] = immutable.merge(schemaProperty, '', schema.defaults);
			}
			else if(schemaProperty.type === 'object'){
				merge(properties[property].properties);
			}
			else if(schemaProperty.type === 'array'){
				if(schemaProperty.items.type === 'object'){
					for(const itemProperty in schemaProperty.items.properties){
						merge(schemaProperty.items.properties[itemProperty]);
					}
				}
				else{
					console.error('not implemented');
				}
			}
		}
	};
	if(!!schema.defaults){
		merge(schema.properties);
	}

	return new JSONSchemaBridge(schema, validadorCallback);
}

export default AppitUniform;
