/**
 * 
 * @returns {{domain, validhost: string, subdomain: (string|string), unknown: boolean}}
 */
export const useAppitDomain = () => {
	const validhost = process.env.REACT_APP_BASE_HOST;
	const baseDomain = validhost.replace(/:[0-9]+/,"");
	const hostname = window.location.hostname;
	const unknown = !hostname.match(baseDomain);
	const subdomain = unknown ? "" : hostname.replace(new RegExp(`\\.?${baseDomain}`),"");
	const domain = unknown ? hostname : hostname.replace(new RegExp(`${subdomain}\\.?`),"");

	return {validhost,subdomain,domain,unknown}
}