export const AppitLinkSettings = {
	globals: [],
	fallbackRoot: (props) => null
}

export const getFixedPath = (path, root) => {
	if(!!path && !(path in AppitLinkSettings.globals) && path !== root){
		path = [root, path].join('/');
	}
	if(path in AppitLinkSettings.globals){
		path = '/' + path;
	}
	if(!path){
		path = '/';
	}

	return path;
}