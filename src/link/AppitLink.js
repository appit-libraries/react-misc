import AppitLinkContext from 'link/AppitLinkContext';
import {AppitLinkSettings, getFixedPath} from 'link/linkUtils';
import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';

class AppitLink extends React.Component {
	static contextType = AppitLinkContext;

	static propTypes = {
		to: PropTypes.string,
		className: PropTypes.string,
	};
	static defaultProps = {
		to: '/',
	};

	render(){
		const root = this.context ? this.context.root : AppitLinkSettings.fallbackRoot(this.props);
		const path = getFixedPath(this.props.to, root);

		return <Link
			to={path}
			className={this.props.className}
		>
			{this.props.children}
		</Link>;
	}
}

export default withRouter(AppitLink);