import React, {createContext, useContext, useEffect, useState} from 'react';
import "./AppitLoader.scss";

const AppitLoaderAwareness = createContext();

export function AppitLoaderAwarenessProvider({children}){
	const [count, setCount] = useState(0);
	let visible = count > 0;

	useEffect(() => {
		const splashScreen = document.getElementById('content-loader');

		// Show SplashScreen
		if(splashScreen && visible){
			splashScreen.classList.remove('hidden');

			return () => {
				splashScreen.classList.add('hidden');
			};
		}

		// Hide SplashScreen
		let timeout;
		if(splashScreen && !visible){
			timeout = setTimeout(() => {
				splashScreen.classList.add('hidden');
			}, 3000);
		}

		return () => {
			clearTimeout(timeout);
		};
	}, [visible]);

	return (
		<AppitLoaderAwareness.Provider value={setCount}>
			{children}
		</AppitLoaderAwareness.Provider>
	);
}

export function AppitLoader({visible = true}){
	// Everything are ready - remove splashscreen
	const setCount = useContext(AppitLoaderAwareness);

	useEffect(() => {
		if(!visible){
			return;
		}
		if(typeof setCount === 'function'){
			setCount(prev => {
				return prev + 1;
			});

			return () => {
				setCount && setCount(prev => {
					return prev - 1;
				});
			};
		}
	}, [setCount, visible]);

	return null;
}

export const AppitLoaderElement = ({logo}) => {
	return (
		<div className="content-loader" id="content-loader" style={{minHeight: '300px'}}>
			<img
				src={logo}
				alt="Carregando..."
			/>
			<svg
				className="splash-spinner primary"
				viewBox="0 0 50 50"
			>
				<circle
					className="path"
					cx="25"
					cy="25"
					r="20"
					fill="none"
					strokeWidth="5"
				/>
			</svg>
		</div>
	);
};

export function AppitWaiting({loading, children = null,loader:Loader=AppitLoader}){
	if(loading){
		return <Loader />;
	}

	return children;
}
