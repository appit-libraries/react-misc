import React, {createContext, useContext, useEffect, useState} from 'react';

const AppitSplashScreenAwareness = createContext();

export function AppitSplashScreenAwarenessProvider({children}){
	const [count, setCount] = useState(0);
	let visible = count > 0;

	useEffect(() => {
		const splashScreen = document.getElementById('splash-screen');

		// Show SplashScreen
		if(splashScreen && visible){
			splashScreen.classList.remove('hidden');

			return () => {
				splashScreen.classList.add('hidden');
			};
		}

		// Hide SplashScreen
		let timeout;
		if(splashScreen && !visible){
			timeout = setTimeout(() => {
				splashScreen.classList.add('hidden');
			}, 3000);
		}

		return () => {
			clearTimeout(timeout);
		};
	}, [visible]);

	return (
		<AppitSplashScreenAwareness.Provider value={setCount}>
			{children}
		</AppitSplashScreenAwareness.Provider>
	);
}

export function AppitSplashScreen({visible = true}){
	// Everything are ready - remove splashscreen
	const setCount = useContext(AppitSplashScreenAwareness);

	useEffect(() => {
		if(!visible){
			return;
		}

		if(setCount){
			setCount(prev => {
				return prev + 1;
			});

			return () => {
				setCount(prev => {
					return prev - 1;
				});
			};
		}
	}, [setCount, visible]);

	return null;
}
