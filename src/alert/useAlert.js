import {useEffect} from 'react';
import {AppitAlertOperation} from './AppitAlert';

export default (dispatch, reducer: ReducerBasicOption, reload,with_state,cond=true) => {
	const state = reducer.useMap();
	useEffect(() => {
		if(cond){
			AppitAlertOperation({
				successAction: with_state ? () => reload(state) : reload,
				always: () => reducer.dispatch_clear(dispatch),
				error: state.error,
				finished: state.finished,
			});
		}
	}, [dispatch, state,reducer, reload,with_state,cond]);
}