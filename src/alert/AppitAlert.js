import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import React from 'react';

export const AppitAlertSettings = {
	confirmButtonClass: '',
	cancelButtonClass: '',
	propError: 'violations',
	toViolation: (v,index) => (<>{v.message} <small>[{v.field}]</small>,</>),
	timeout: 300,
};

export const AppitAlert = (settings = {}, then = () => null) => {
	const SwalReact = withReactContent(Swal);

	setTimeout(() => SwalReact.fire({
			customClass: {
				confirmButton: AppitAlertSettings.confirmButtonClass,
				cancelButton: AppitAlertSettings.cancelButtonClass,
				// title: 'display-4',
				...settings.customClass,
			},
			cancelButtonText: 'Voltar',
			width: '40em',
			heightAuto: false,
			// grow: true,
			buttonsStyling: false,
			...settings,
		},
	).then(then), AppitAlertSettings.timeout);
};

export const AppitAlertBasic = (title, message, then = () => null) => AppitAlert({
	title: title,
	html: message,
}, then);

export const AppitAlertConfirm = (title, message, action = () => null,elseAction=() => null) => {
	AppitAlert(
		{
			title: !!title ? title : 'Aviso',
			cancelButtonText: 'Cancelar',
			showCancelButton: true,
			confirmButtonText: 'Confirmar',
			html: message,
		},
		(object) => {
			// console.log(object)
			const {value} = object;
			if(value){
				action();
			}
			else{
				elseAction();
			}
		},
	);
}

export const AppitAlertOperation = (
	{
		successTitle, successMessage, successAction,
		failureTitle, failureMessage, failureAction,
		error,
		finished,
		always,
	},
) => {
	if(!successTitle){
		successTitle = 'Operação realizada com sucesso';
	}
	if(!failureTitle){
		failureTitle = 'Operação não foi realizada';
	}
	if(!successAction){
		successAction = () => null;
	}
	if(!failureAction){
		failureAction = () => null;
	}
	const callIfFunction = a => typeof a === 'function' ? a() : a;
	if(finished){
		// console.log("ola mundo")
		if(error){
			let fallbackMessage = failureMessage;
			console.log(error)
			if(!fallbackMessage && !!error && error.isAxiosError && error.response.data){
				fallbackMessage = [];
				if(error.response.data.error){
					let message = error.response.data[AppitAlertSettings.propError];
					if(!Array.isArray(message)){
						message = [message]
					}
					fallbackMessage = message.map((v,index) => (
						<li key={`violation-${index}`}>
							{AppitAlertSettings.toViolation(v,index)}
						</li>
					))
					fallbackMessage = <ul>{fallbackMessage}</ul>
				}
			}
			AppitAlertBasic(
				callIfFunction(failureTitle),
				callIfFunction(fallbackMessage),
				failureAction,
			);
		}
		else{
			AppitAlertBasic(
				callIfFunction(successTitle),
				callIfFunction(successMessage),
				successAction,
			);
		}
		callIfFunction(always);
	}
};
