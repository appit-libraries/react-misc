import React from 'react';
import Form from 'react-bootstrap/Form';
import {ErrorMessage, Field} from 'formik';
import PropTypes from '@types/prop-types';

/**
 *
 * @param {string} name
 * @param {string} className
 * @param {string} label
 * @param {placeholder} label
 * @param type
 * @param {string} size
 * @param children
 * @param component
 * @param componentProps
 * @param {boolean} withoutGroup
 * @param {boolean} withoutLabel
 * @param beforeControl
 * @param {string} placeholder
 * @returns {*}
 * @constructor
 */
const j = ({name, className = '', label, type = 'string', size = 'lg',children, component,componentProps={}, withoutGroup = false, withoutLabel = false, beforeControl = (a) => a,placeholder}) => {

};

export default class FormElementField extends React.Component{
	static propTypes = {
		name: PropTypes.string.isRequired,
		className: PropTypes.string,
		label: PropTypes.any,
		type: PropTypes.string,
		component,
		componentProps: PropTypes.object,
		withoutGroup: PropTypes.boolean,
		withoutLabel: PropTypes.boolean,
		beforeControl: PropTypes.func,
		placeholder: PropTypes.string
	}
	static defaultProps = {
		beforeControl: (Component) => Component
	}

	getGroupComponent(){
		return 'div';
	}

	getLabelComponent(){
		return 'label';
	}

	get Component(){
		if(this.props.withoutGroup){
			if(this.props.className){
				throw new Error("Without groups should not have className")
			}
			return React.Fragment
		}

		return this.getGroupComponent()
	}

	get Label(){

		return this.getLabelComponent();
	}

	render(): React.ReactNode{
		const props = {};
		if(Element === 'div'){
			props.className = className;
		}
		if(!!placeholder){
			componentProps.placeholder = placeholder;
		}
		if(!!children){
			componentProps.children = children;
		}

		return (
			<Component {...props}>
				{
					!withoutLabel && (
						<Label className="text-capitalize">{label ? label : name}</Label>
					)
				}

				{
					(this.props.beforeControl(<Form.Control
						size={size}
						className={`Control ${type === 'file' ? 'custom-file-input' : ''}`}
						as={Field}
						component={component}
						type={type}
						name={name}
						placeholder={label ? label : name}
						id={`input_${name}`}
						{...componentProps}
					/>))
				}
				<ErrorMessage name={name} component="div" />
			</Component>
		);
	}
}