import React from 'react';
import {Formik, FormikProps} from 'formik';
import PropTypes from 'prop-types';

class FormElement extends React.Component {
	static propTypes = {
		validate: PropTypes.func,
		onSubmit: PropTypes.func,
		initialValues: PropTypes.any,
		submitProps: PropTypes.any,
		submitText: PropTypes.any,
		submittingText: PropTypes.any,
		hideSubmit: PropTypes.bool,
		asFunc: PropTypes.bool,
	};
	static defaultProps = {
		submitText: 'Enviar',
		submittingText: 'Enviando...',
		submitProps: {},
		submittingProps: {},
		initialValues: {},
		className: '',
		hideSubmit: false,
		asFunc: false,
		validate: () => ({}),
	};

	FormComponent(){
		return 'form'
	}

	SubmitComponent(){
		return 'button'
	}

	render(){
		const Submit = this.SubmitComponent();
		const Form = this.FormComponent();

		return (
			<Formik
				validate={this.props.validate}
				onSubmit={this.props.onSubmit}
				initialValues={this.props.initialValues}
			>
				{
					(formikProps:FormikProps) => {
						const {isSubmitting, handleSubmit,values,setFieldValue} = formikProps;
						let text = this.props.submitText;
						let props = this.props.submitProps;
						if(isSubmitting){
							text = this.props.submittingText;
							props = this.props.submittingProps;
						}

						return (
							<Form
								onSubmit={handleSubmit}
								className={`form ${this.props.className}`}
							>
								{this.props.asFunc ? this.props.children(formikProps) : this.props.children}
								{
									!this.props.hideSubmit && (
										<Submit
											type="submit"
											disabled={isSubmitting} {...props}>
											{text}
										</Submit>
									)
								}
							</Form>
						);
					}
				}
			</Formik>
		);
	}
}

export default FormElement;