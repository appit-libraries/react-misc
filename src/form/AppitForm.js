import React from 'react';
import PropTypes from 'prop-types';
import {AppitAlertBasic} from 'alert/AppitAlert';

export default class AppitForm extends React.Component {
	static propTypes = {
		onSubmit: PropTypes.func,
		afterSuccessAlert: PropTypes.func,
		success: PropTypes.bool,
		error: PropTypes.any,
		children: PropTypes.element
	};

	static defaultProps = {
		onSubmit: (data) => null,
		afterSuccessAlert: () => window.location.reload(),
		error: null,
	};

	state = {
		showError: false
	}

	showingForm = true;
	finished = false;
	hideForm = () => this.showingForm = false;
	showForm = () => this.showingForm = true;
	isShowing = () => this.showingForm;
	showError = () => this.setState({showError: true});
	hideError = () => this.setState({showError: false});
	setSubmitting = (b) => null;

	handleData = (data) => data;

	onSubmit = (values, {setSubmitting}) => {
		const handledValues = this.handleData(values);
		this.props.onSubmit(handledValues);
		this.hideForm();
		this.setSubmitting = setSubmitting;
		this.showError();
		this.finished = false
	};
	getAlertTitle(){
		return 'Operação realizada com sucesso';
	}

	getAlertMessage(){
		return 'Os dados foram salvos com sucesso';
	}

	getAlertErrorTitle(){
		return 'Operação não foi realizada';
	}

	getErrorPropName(name){
		return null;
	}

	errorConverter(error={}){
		throw new Error("Error converter must be implemented")
	}

	componentDidUpdate(prevProps, prevState, snapshot){
		if(this.props.success && !this.isShowing()){
			AppitAlertBasic(
				this.getAlertTitle(),
				this.getAlertMessage(),
				() => {
					this.showForm();
					this.props.afterSuccessAlert();
					this.finished = true;
				},
			);
		}
		if(!!this.props.error && this.state.showError){
			const msgs = this.errorConverter(this.props.error)

			AppitAlertBasic(this.getAlertErrorTitle(), msgs.join('<br>'), () => {
				this.showForm();
				this.hideError();
				this.finished = true;
			});
		}
	}

	Form(){
		return null;
	}

	Loader(){
		return "Loading..."
	}

	render(){
		if(this.isShowing() || this.finished){
			return this.form();
		}

		return this.Loader();
	}
}