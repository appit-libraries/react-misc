import * as React from 'react';
import {Formik, Form, Field, FieldProps, ErrorMessage, FieldInputProps, FormikProps} from 'formik';
import Select from 'react-select';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';

export const FormElementFieldSelect = ({
										   field, // { name, value, onChange, onBlur }
										   form: {touched, errors, setFieldValue,values}, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
										   withoutLabel,
										   ...props
									   }: { options: Array<{ value: string; label: string }>; field: FieldInputProps<V>; form: FormikProps<FormValues>; }) => {
	const {options,size="lg"} = props;
	props.className = !!props.className ? props.className : "";
	props.className += ` select mb-3 form-control form-control-${size} mb-3`;
	props.classNamePrefix = "select";
	const currentOption = options ? options.find(option => option.value === field.value) : '';

	return (
		<FormGroup>
			{!withoutLabel && (<FormLabel htmlFor={field.name}>{props.label}</FormLabel>) }
			<Select
				{...field}
				{...props}
				options={options}
				value={currentOption}
				onChange={option => {
					setFieldValue(field.name, option.value)
					if(typeof field.onChange === 'function'){
						field.onChange(option)
					}
				}}
			/>
			{touched[field.name] && errors[field.name] && (
				<ErrorMessage name={field.name}>{errors[field.name]}</ErrorMessage>
			)}
		</FormGroup>
	);
};