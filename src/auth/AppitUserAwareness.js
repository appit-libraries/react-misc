import React, {createContext, useContext} from 'react';


export const AppitUserAwarenessContext = createContext();

export function useAppitUserAwareness() {
	return useContext(AppitUserAwarenessContext);
}

export function withAppitUserAwareness(Component) {
	class withAppitUserAwareness extends React.Component {
		static displayName = `WithAppitUserAwareness(${Component.displayName || Component.name})`;

		static contextType = AppitUserAwarenessContext;

		render() {
			return <Component {...this.props} appitUserAwareness={this.context} />;
		}
	}

	return withAppitUserAwareness;
}

export const AppitUserAwarenessConsumer = AppitUserAwarenessContext.Consumer;
export const AppitUserAwarenessProvider = AppitUserAwarenessContext.Provider;