import React, {createContext, useContext} from 'react';


export const AppitTokenAwarenessContext = createContext();

export function useAppitTokenAwareness() {
	return useContext(AppitTokenAwarenessContext);
}

export function withAppitTokenAwareness(Component) {
	class withAppitTokenAwareness extends React.Component {
		static displayName = `WithAppitTokenAwareness(${Component.displayName || Component.name})`;

		static contextType = AppitTokenAwarenessContext;

		render() {
			return <Component {...this.props} appitTokenAwareness={this.context} />;
		}
	}

	return withAppitTokenAwareness;
}

export const AppitTokenAwarenessConsumer = AppitTokenAwarenessContext.Consumer;
export const AppitTokenAwarenessProvider = AppitTokenAwarenessContext.Provider;