import PropTypes from 'prop-types';
import React, {useEffect} from 'react';
import {isElement} from 'react-is';
import {AppitUserAwarenessProvider} from './AppitUserAwareness';

const stateType = PropTypes.shape({
	finished: PropTypes.bool,
	begined: PropTypes.bool,
	data: PropTypes.any,
	error: PropTypes.any,
});

AppitRouter.propTypes = {
	refreshToken: PropTypes.func.isRequired,
	loadTokenFromStorage: PropTypes.func,
	token: PropTypes.string,
	tokenState: stateType.isRequired,
	onTokenError: PropTypes.func,
	loginState: stateType.isRequired,
	onLoginError: PropTypes.func,
	logoutState: stateType.isRequired,
	onLogoutFinished: PropTypes.func,

	loader: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.elementType,
	]),
	authView: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.elementType,
	]),
	force: PropTypes.bool,
};
AppitRouter.defaultProps = {
	force: false,
	authViewProps: {},
};

function AppitRouter({
						 tokenState,
						 loginState,
						 logoutState,
						 onLoginError,
						 onTokenError,
						 onLogoutFinished,
						 refreshToken,
						 authView: AuthView,
						 force,
						 loader: Loader,
						 children,
						 token,
						 loadTokenFromStorage,
					 }){

	const {
		// touched: tokenTouched,
		error: tokenError,
		finished: tokenFinished,
		// begined: tokenBegined,
		data: user,
	} = tokenState

	const {
		// touched: userTouched,
		error: loginError,
		// finished: loginFinished,
		// begined: loginBegined,
	} = loginState
	const  {
		finished: logoutFinished,
	} = logoutState

	const callIfFunction = fn => typeof fn === 'function' ? fn() : fn;

	const [waitForToken, setWaitForToken] = React.useState(!!token || !!callIfFunction(loadTokenFromStorage));
	const [previousUser, setPreviousUser] = React.useState(null);
	const [handledTokenError, setHandledTokenError] = React.useState(false);
	const [handledLoginError, setHandledLoginError] = React.useState(false);
	const [handledLogout, setHandledLogout] = React.useState(false);

	// console.info("token",token)
	// console.info("waitForToken",waitForToken)

	useEffect(() => {
		setWaitForToken(!!token);
		setHandledTokenError(false);
		setHandledLoginError(false);
		setHandledLogout(false);
	}, [token]);

	useEffect(() => {
		setHandledTokenError(false);
	}, [tokenState.clear]);

	useEffect(() => {
		setHandledLoginError(false);
	}, [loginState.clear]);

	useEffect(() => {
		setHandledLogout(false);
	}, [logoutState.clear]);

	useEffect(() => {
		if(tokenFinished){
			setPreviousUser(user);
		}
	}, [tokenFinished, user]);

	useEffect(() => {
		if(!loginError && waitForToken && !force){
			refreshToken();
		}
	}, [loginError, refreshToken, waitForToken, force]);

	useEffect(() => {
		if(force || handledLoginError){
			return;
		}
		if(!!loginError){
			// console.log('login')
			if(typeof onLoginError === 'function'){
				onLoginError(loginError);
			}
			setHandledLoginError(true);
		}
	}, [force, loginError, onLoginError, handledLoginError]);

	useEffect(() => {
		if(force || handledTokenError){
			return;
		}
		if(!!tokenError){
			if(!!onTokenError){
				onTokenError(tokenError);
			}

			setWaitForToken(false);
			setHandledTokenError(true);
		}
	}, [force, tokenError, onTokenError, handledTokenError]);

	useEffect(() => {
		if(force || handledLogout){
			return;
		}
		if(logoutFinished){
			// console.log('logout')
			if(!!onLogoutFinished){
				onLogoutFinished(previousUser);
			}
			setWaitForToken(false);
			setHandledLogout(true);
		}
	}, [force, logoutFinished, onLogoutFinished, previousUser, handledLogout]);

	if(!force){
		if(loginError || logoutFinished || !waitForToken){

			// console.log('unauthenticated');
			if(isElement(AuthView)){
				return AuthView;
			}
			return <AuthView previousUser={previousUser} fromLogout={logoutFinished} />;
		}

		if(!user){
			// console.log('loading');
			if(!Loader){
				return null;
			}
			if(isElement(Loader)){
				return Loader;
			}
			return <Loader />;
		}
	}

	// console.log('authenticated');
	return <AppitUserAwarenessProvider value={user}>
		{children}
	</AppitUserAwarenessProvider>;
}

export default AppitRouter;