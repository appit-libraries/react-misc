import React, {createContext, useContext,useEffect, useState,useCallback} from 'react';


const PagarmeAwarenessContext = createContext();

export function usePagarmeAwareness() {
	return useContext(PagarmeAwarenessContext);
}

export function withPagarmeAwareness(Component) {
	class withPagarmeAwareness extends React.Component {
		static displayName = `WithPagarmeAwareness(${Component.displayName || Component.name})`;

		static contextType = PagarmeAwarenessContext;

		render() {
			return <Component {...this.props} pagarmeAwareness={this.context} />;
		}
	}

	return withPagarmeAwareness;
}

export const PagarmeAwarenessConsumer = PagarmeAwarenessContext.Consumer;

type CardType =  {
	card_holder_name: string,
	card_expiration_date: string,
	card_number: string,
	card_cvv: string,
};

/**
 *
 * @param children
 * @param isSandbox
 * @param {CardType} card
 * @returns {*}
 * @constructor
 */
export const PagarmeAwarenessProvider = ({children,isSandbox,encryption_key=process.env.REACT_APP_PAGARME_ENC}) => {
	const [loaded, setLoaded] = useState(false);
	isSandbox = isSandbox === undefined ? true : isSandbox;

	useEffect(() => {
		let script = document.getElementById('pagarme');
		if(!script){
			script = document.createElement('script');
			script.id = 'pagarme';
			script.type = 'text/javascript';
			script.src = `https://assets.pagar.me/pagarme-js/4.5/pagarme.min.js`;
			script.onload = () => {
				setLoaded(true);
			};
			document.body.appendChild(script);
		}
		else{
			setLoaded(true);
		}
	}, []);

	const generateToken = useCallback(async (card:CardType,onError) => {
		if(!loaded){
			throw new Error()
		}
		const validations = window.pagarme.validate({card})

		for(const field of Object.keys(validations.card)){
			if(!validations.card[field]){
				if(onError){
					onError(field,validations.card[field])
				}
				throw new Error(field)
			}
		}

		const client = await window.pagarme.client.connect({encryption_key})
		const hash = await client.security.encrypt(card)

		return hash;
	},[loaded,encryption_key])

	return (
		<PagarmeAwarenessContext.Provider value={{generateToken}}>
			{children}
		</PagarmeAwarenessContext.Provider>
	)
};