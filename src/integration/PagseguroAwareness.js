import PropTypes from 'prop-types';
import React, {createContext, useContext, useEffect, useState} from 'react';


const PagseguroAwarenessContext = createContext();
type OnSenderHasReadyType = {
	status: string;
	message: string;
	senderHash: string;
};
type CreateCardTokenType = {
	cardNumber: string;
	brand: string;
	cvv: string;
	expirationMonth: string;
	expirationYear: string;
	success: ({ card: { token: string } }) => any;
	error: (response: Object) => any;
	complete: (response: Object) => any;
}

type GetBrandType = {
	cardBin: string;
	success: (response:Object) => any;
	error: (response:Object) => any;
	complete: (response:Object) => any;
}

export type PagseguroType = {
	onSenderHashReady: OnSenderHasReadyType => any;
	createCardToken: CreateCardTokenType => any;
	getBrand: GetBrandType => any;
	promiseBrand: (cardBind:string) => Promise
}

/**
 *
 * @returns {PagseguroType}
 */
export function usePagseguroAwareness(){
	return useContext(PagseguroAwarenessContext);
}

export function withPagseguroAwareness(Component){
	class withPagseguroAwareness extends React.Component {
		static displayName = `WithPagseguroAwareness(${Component.displayName || Component.name})`;

		static contextType = PagseguroAwarenessContext;

		render(){
			return <Component {...this.props} pagseguroAwareness={this.context} />;
		}
	}

	return withPagseguroAwareness;
}
export const getPagseguroCheckoutUrl = (code,isSandbox) => {
	isSandbox = isSandbox === undefined ? process.env.REACT_APP_APPIT_PAGSEGURO_SANDBOX : isSandbox;

	return `https://${isSandbox ? "sandbox." : ""}pagseguro.uol.com.br/v2/checkout/payment.html?code=${code}`;
}

export const PagseguroAwarenessConsumer = PagseguroAwarenessContext.Consumer;
export const PagseguroAwarenessProvider = ({children, isSandbox, onLoad, sessionID}) => {
	const [loaded, setLoaded] = useState(false);
	isSandbox = isSandbox === undefined ? process.env.REACT_APP_APPIT_PAGSEGURO_SANDBOX : isSandbox;

	useEffect(() => {
		let script = document.getElementById('pagseguro-v2');
		if(!script){
			script = document.createElement('script');
			script.id = 'pagseguro-v2';
			script.type = 'text/javascript';
			script.src = `https://stc${isSandbox ? '.sandbox' : ''}.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js`;
			script.onload = () => {
				if(typeof onLoad === 'function'){
					onLoad();
				}
				window.PagSeguroDirectPayment.setSessionId(sessionID);
				setLoaded(true);
			};
			document.body.appendChild(script);
		}
		else{
			if(typeof onLoad === 'function'){
				onLoad();
			}
			setLoaded(true);
		}
	}, [isSandbox,sessionID,onLoad]);

	const pagseguro = window.PagSeguroDirectPayment || {};
	const promiseBrand = (cardBin) => {
		return new Promise((resolve, reject) => {
			if(!pagseguro.getBrand){
				reject();
			}
			cardBin = cardBin.replace(/[\s|.]/g, '');
			if(cardBin.length > 5){
				pagseguro.getBrand({
					cardBin,
					success: ({brand:{name}}) => {
						console.log(name);
						resolve(name);
					},
					error: (response) => {
						// console.log(response);
						reject(response)
						// console.log(return_ps)
					},
					complete: (response) => {
						// console.log(response);
						// console.log(return_ps)
					},
				});
			}
			else{
				resolve("")
			}
		})
	};
	pagseguro.promiseBrand = promiseBrand;

	return (
		<PagseguroAwarenessContext.Provider value={pagseguro}>
			{loaded && children}
		</PagseguroAwarenessContext.Provider>
	);
};
PagseguroAwarenessProvider.propTypes = {
	sessionID: PropTypes.string.isRequired,
};

export const getPageseguroBrandImage = (cardBrand) => {
	const image = `https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/${cardBrand}.png`;

	console.log(cardBrand)
	console.log(image)

	return image;
}